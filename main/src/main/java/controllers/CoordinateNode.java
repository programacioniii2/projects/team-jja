package controllers;

import kruskal.graph.Node;

public class CoordinateNode {
    private double x;
    private double y;
    private Node node;

    public CoordinateNode(Node node) {
        this.node = node;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    @Override
    public String toString() {
        return "CoordinateNode{" +
                "x=" + x +
                ", y=" + y +
                ", node=" + node.getElement() +
                '}';
    }
}
