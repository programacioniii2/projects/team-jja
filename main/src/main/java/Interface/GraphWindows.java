package Interface;

import controllers.GraphController;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import kruskal.graph.Node;

import static Interface.WrapperInterface.customButton;

public class GraphWindows {

    Stage stage;
    private static final int WINDOW_HEIGHT = 300;
    private static final int WINDOW_WIDTH = 600;
    private VBox root;

    public GraphWindows() {
        initStructure();
    }

    private void initStructure() {
        Platform.runLater(() -> {
            stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            root = new VBox();
            root.setSpacing(30);
            root.setStyle("-fx-background-color: rgb(255, 255, 255, 255)");
            Scene scene = new Scene(root, WINDOW_WIDTH, WINDOW_HEIGHT);
            stage.setScene(scene);
        });
    }

    public void addNodeWindow(GraphController graphController) {
        root.getChildren().clear();
        TextField addNodeField = new TextField();
        addNodeField.setFont(Font.font("Arial", 20));
        EventHandler<ActionEvent> event1 = event -> {
            graphController.getGraph().addNode(new Node(addNodeField.getText()));
            graphController.getCoordinateNodes().clear();
            graphController.fillCoordinatesNode();
            stage.close();
        };
        EventHandler<ActionEvent> event2 = event -> {
            stage.close();
        };
        HBox options = drawOptions(event1, event2);
        StackPane header = drawHeader("Please, enter a value for node");

        root.getChildren().addAll(header, addNodeField, options);
        stage.showAndWait();
    }

    public void addEdgeWindow(GraphController graphController) {
        root.getChildren().clear();
        StackPane header = drawHeader("Please, Enter a values for the new edge");
        TextField node1 = new TextField();
        TextField node2 = new TextField();
        TextField weight = new TextField();
        HBox nodes = drawTextField(node1, node2, weight);

        EventHandler<ActionEvent> event = event1 -> {
            Node node11 = graphController.getNode(node1.getText());
            Node node12 = graphController.getNode(node2.getText());
            if (!graphController.getGraph().addEdge(node11, node12, Integer.parseInt(weight.getText()))) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("error");
                alert.setContentText("we cant add the edge");
                alert.showAndWait();
            } else {
                graphController.getGraph().addEdge(node11, node12, Integer.parseInt(weight.getText()));
                graphController.getCoordinateNodes().clear();
                graphController.fillCoordinatesNode();
            }
            stage.close();
        };

        EventHandler<ActionEvent> event2 = event1 -> stage.close();

        HBox options = drawOptions(event, event2);

        node1.setFont(Font.font("Arial", 20));
        node2.setFont(Font.font("Arial", 20));
        weight.setFont(Font.font("Arial", 20));

        root.getChildren().addAll(header, nodes, options);

        stage.showAndWait();
    }

    private HBox drawTextField(TextField node1, TextField node2, TextField weight) {
        HBox nodes = new HBox();
        VBox node1Info = new VBox();
        VBox node2Info = new VBox();
        VBox weightInfo = new VBox();
        Label node1Text = new Label("node 1");
        Label node2Text = new Label("node 2");
        Label weightText = new Label("Weight");

        node1Text.setFont(Font.font("Arial", 17));
        node2Text.setFont(Font.font("Arial", 17));
        weightText.setFont(Font.font("Arial", 17));

        node1Info.getChildren().addAll(node1Text, node1);
        node2Info.getChildren().addAll(node2Text, node2);
        weightInfo.getChildren().addAll(weightText, weight);

        node1Info.setSpacing(10);
        node2Info.setSpacing(10);
        weightInfo.setSpacing(10);
        nodes.setSpacing(15);
        nodes.getChildren().addAll(node1Info, node2Info, weightInfo);

        return nodes;
    }

    private StackPane drawHeader(String text) {
        StackPane header = new StackPane();
        Rectangle background = new Rectangle(WINDOW_WIDTH, 70);
        Label title = new Label(text);

        title.setFont(Font.font("Arial", 20));
        title.setTextFill(Color.WHITE);
        background.setFill(Color.valueOf("#1c66bd"));

        header.setAlignment(Pos.CENTER);
        header.getChildren().addAll(background, title);

        return header;
    }

    private HBox drawOptions(EventHandler<ActionEvent> event, EventHandler<ActionEvent> event2) {
        HBox options = new HBox();

        Button accept = customButton("Accept", 20);
        Button cancel = customButton("Cancel", 20);
        accept.setOnAction(event);
        cancel.setOnAction(event2);

        options.getChildren().addAll(accept, cancel);
        options.setSpacing(50);

        return options;
    }
}
