package kruskal;

import java.util.*;

public class Kruskal {
  Map<Integer, Integer> parent = new HashMap<>();

  // realizar la operación MakeSet
  public void makeSet(int n) {
    // crea `n` conjuntos disjuntos (uno para cada vértice)
    for (int i = 0; i < n; i++) {
      parent.put(i, i);
    }
  }

  // Encuentra la raíz del conjunto al que pertenece el elemento `k`
  private int find(int k) {
    // si `k` es root
    if (parent.get(k) == k) {
      return k;
    }

    // recurre para el padre hasta que encontramos la raíz
    return find(parent.get(k));
  }

  // Realizar Unión de dos subconjuntos
  private void union(int a, int b) {
    // encontrar la raíz de los conjuntos a los que pertenecen los elementos `x` e `y`
    int x = find(a);
    int y = find(b);

    parent.put(x, y);
  }

  // Función para construir MST usando el algoritmo de Kruskal
  public static List<Edge2> runKruskalAlgorithm(List<Edge2> edge2s, int n) {
    // almacena los bordes presentes en MST
    List<Edge2> MST = new ArrayList<>();

    // Inicializa la clase `DisjointSet`.
    // crea un conjunto singleton para cada elemento del universo.
    Kruskal ds = new Kruskal();
    ds.makeSet(n);

    int index = 0;

    // ordena los bordes aumentando el peso
    edge2s.sort(Comparator.comparingInt(e -> e.weight));

    // MST contiene exactamente los bordes `V-1`
    while (MST.size() != n - 1)
    {
      // considera el siguiente borde con peso mínimo del graph
      Edge2 next_edge2 = edge2s.get(index++);

      // encuentra la raíz de los conjuntos a los que pertenecen dos extremos
      // los vértices de la siguiente arista pertenecen
      int x = ds.find(next_edge2.src);
      int y = ds.find(next_edge2.dest);

      // si ambos extremos tienen diferentes padres, pertenecen a
      // diferentes componentes conectados y se pueden incluir en MST
      if (x != y)
      {
        MST.add(next_edge2);
        ds.union(x, y);
      }
    }
    return MST;
  }
}