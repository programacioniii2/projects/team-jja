package kruskal;

import controllers.GraphController;
import kruskal.graph.Edge;
import kruskal.graph.Graph;
import kruskal.graph.Node;
import kruskal.graph.UndirectedGraph;
import org.junit.jupiter.api.Test;

import static Interface.WrapperInterface.createGraph;
import static Interface.WrapperInterface.layoutGraph;

class KruskalTest {
  @Test
  public void test() {
    // (u, v, w) el triplete representa el borde no dirigido desde
    // vértice `u` a vértice `v` con peso `w`
    List<Edge2> edge2s = Arrays.asList(
            new Edge2(0, 1, 7), new Edge2(1, 2, 8), new Edge2(0, 3, 5),
            new Edge2(1, 3, 9), new Edge2(1, 4, 7), new Edge2(2, 4, 5),
            new Edge2(3, 4, 15), new Edge2(3, 5, 6), new Edge2(4, 5, 8),
            new Edge2(4, 6, 9), new Edge2(5, 6, 11));

    // número total de nodos en el graph (etiquetados de 0 a 6)
    int n = 7;

    // construir grafo
    List<Edge2> e = Kruskal.runKruskalAlgorithm(edge2s, n);
    System.out.println(e);
  }

  @Test
  public void graphController() {
    Graph graph = new UndirectedGraph();
    Node node1 = new Node("a");
    Node node2 = new Node("b");
    Node node3 = new Node("c");
    Node node4 = new Node("d");
    Node node5 = new Node("e");

    Edge edge1 = new Edge(node1, node2, 20);
    Edge edge2 = new Edge(node2, node3, 10);
    Edge edge3 = new Edge(node3, node4, 14);
    Edge edge4 = new Edge(node4, node5, 5);
    Edge edge5 = new Edge(node1, node3, 21);
    Edge edge6 = new Edge(node2, node4, 12);

    graph.addNode(node1);
    graph.addNode(node2);
    graph.addNode(node3);
    graph.addNode(node4);
    graph.addNode(node5);

    graph.addEdge(edge1);
    graph.addEdge(edge2);
    graph.addEdge(edge3);
    graph.addEdge(edge4);
    graph.addEdge(edge5);
    graph.addEdge(edge6);

    Kruskal2 kruskal2 = new Kruskal2(graph);

    Graph a = kruskal2.kruskalAlgorithm();

    System.out.println(a.getAdjacencySets());
  }

  @Test
  public void a() {
    GraphController a = new GraphController(createGraph());
    layoutGraph(a.getCoordinateNodes(), 200, 200, 200, 10);
    a.getGraph().addNode(new Node("asd"));
    a.getGraph().addNode(new Node("as"));
    a.getCoordinateNodes().clear();
    a.fillCoordinatesNode();

    Node as = a.getNode("a");

    System.out.println(as.getElement());
  }
}