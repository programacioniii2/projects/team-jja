package kruskal.graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public abstract class Graph {

  protected Map<Node, Set<Edge>> adjacencySets;
  private int numNodes;
  protected int numEdges;

  public Graph() {
    adjacencySets = new HashMap<>();
    numNodes = 0;
    numEdges = 0;
  }

  public Set<Edge> getNode(Node node) {
    return adjacencySets.get(node);
  }

  public int getNumNodes() {
    return this.numNodes;
  }

  public int getNumEdges() {
    return this.numEdges;
  }

  public boolean containsNode(Node node) {
    return adjacencySets.containsKey(node);
  }

  public boolean addNode(Node newNode) {
    if (newNode == null || this.containsNode(newNode)) {
      return false;
    }
    Set<Edge> newAdjacencySet = new HashSet<>();
    adjacencySets.put(newNode, newAdjacencySet);
    numNodes++;
    return true;
  }

  public Map<Node, Set<Edge>> getAdjacencySets() {
    return adjacencySets;
  }

  public Set<Node> getNodeNeighbors(Node node) {
    if (!containsNode(node)) {
      return null;
    }
    Set<Edge> nodeEdges = adjacencySets.get(node);
    Set<Node> nodeNeighbors = new HashSet<>();
    for (Edge e : nodeEdges) {
      Node neighbor = e.getDestination();
      nodeNeighbors.add(neighbor);
    }
    return nodeNeighbors;
  }

  public abstract boolean addEdge(Node node1, Node node2, int weight);
  public abstract boolean addEdge(Edge edge);

  public abstract boolean removeEdge(Node node1, Node node2, int weight);

  public abstract boolean updateEdge(Node node1, Node node2, int weight, int newWeight);

  public boolean addEdgeFromTo(Node source, Node destination, int weight) {
    Edge newEdge = new Edge(source, destination, weight);
    Set<Edge> sourceEdges = adjacencySets.get(source);
    if (!sourceEdges.contains(newEdge)) {
      sourceEdges.add(newEdge);
      return true;
    }
    return false;
  }

  public boolean removeNode(Node node) {
    if (!adjacencySets.containsKey(node)) {
      return false;
    }
    for (Set<Edge> edges : this.adjacencySets.values()) {
      if (edges.removeIf(entry -> entry.getSource().equals(node) || entry.getDestination().equals(node))) {
        numEdges--;
      }
    }
    this.adjacencySets.remove(node);
    numNodes--;
    return true;
  }

  public Edge getEdge(Node source, Node destination, int weight) {
    Edge edge = new Edge(source, destination, weight);
    Set<Edge> sourceEdges = adjacencySets.get(source);
    return sourceEdges.stream().filter(e -> e.equals(edge)).findFirst().orElseThrow();
  }
}
