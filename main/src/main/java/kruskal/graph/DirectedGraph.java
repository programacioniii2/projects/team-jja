package kruskal.graph;

import java.util.Iterator;
import java.util.Set;

public class DirectedGraph extends Graph {

  public DirectedGraph() {
    super();
  }

  @Override
  public boolean addEdge(Node source, Node dest, int weight) {
    addNode(source);
    addNode(dest);
    boolean addSuccess = addEdgeFromTo(source, dest, weight);
    if (addSuccess) {
      super.numEdges++;
    }
    return addSuccess;
  }

  @Override
  public boolean addEdge(Edge edge) {
    Node source = edge.getSource();
    Node dest= edge.getDestination();
    int weight = edge.getWeight();
    addNode(source);
    addNode(dest);
    boolean addSuccess = addEdgeFromTo(source, dest, weight);
    if (addSuccess) {
      super.numEdges++;
    }
    return addSuccess;
  }

  @Override
  public boolean removeEdge(Node node1, Node node2, int weight) {
    Edge e1 = new Edge(node1, node2, weight);
    boolean result = false;
    Iterator<Node> it = this.adjacencySets.keySet().iterator();
    while (it.hasNext() && !result) {
      Node current = it.next();
      Set<Edge> edges = this.adjacencySets.get(current);
      result = edges.removeIf(e -> e.equals(e1));
    }
    return result;
  }

  @Override
  public boolean updateEdge(Node node1, Node node2, int weight, int newWeight) {
    Edge e1 = new Edge(node1, node2, weight);
    for (Node current : this.adjacencySets.keySet()) {
      Set<Edge> edges = this.adjacencySets.get(current);
      if (edges.contains(e1)) {
        edges.stream().filter(e -> e.equals(e1)).findFirst().orElseThrow().setWeight(newWeight);
        return true;
      }
    }
    return false;
  }
}
