package prim;

import org.junit.jupiter.api.Test;

public class PrimTest {
  @Test
  public void testPrim() {
    int vertices = 6;
    Prim.Grafo grafo = new Prim.Grafo(vertices);
    grafo.addEdge(0, 1, 10);
    grafo.addEdge(0, 2, 25);
    grafo.addEdge(1, 4, 30);
    grafo.addEdge(1, 2, 10);
    grafo.addEdge(2, 5, 5);
    grafo.addEdge(2, 3, 20);
    grafo.addEdge(3, 5, 40);
    grafo.addEdge(5, 4, 12);
    grafo.mst();
  }
}