package kruskal.graph;

public class Edge {

  private final Node source;
  private final Node destination;
  private int weight;

  public Edge(Node source, Node destination, int weight) {
    this.source = source;
    this.destination = destination;
    this.weight = weight;
  }

  public String toString() {
    return "(" + this.source.toString() + "," + this.destination.toString() + "," + this.weight + ")";
  }

  public Node getSource() {
    return this.source;
  }

  public Node getDestination() {
    return this.destination;
  }

  public int getWeight() {
    return this.weight;
  }

  public void setWeight(int weight) {
    this.weight = weight;
  }

  @Override
  public boolean equals(Object o) {
    Edge otherEdge = (Edge) o;
    Node otherSource = otherEdge.getSource();
    Node otherDest = otherEdge.getDestination();
    int otherWeight = otherEdge.getWeight();
    return (otherSource.equals(source) && otherDest.equals(destination) && otherWeight == weight);
  }

  @Override
  public int hashCode() {
    return source.hashCode() + destination.hashCode() + this.weight;
  }
}
