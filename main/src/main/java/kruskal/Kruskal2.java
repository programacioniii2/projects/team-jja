package kruskal;

import kruskal.graph.Edge;
import kruskal.graph.Graph;
import kruskal.graph.Node;
import kruskal.graph.UndirectedGraph;

import java.util.*;

public class Kruskal2 {
    HashMap<Node, Set<Node>> parent;
    Graph graph;
    public Kruskal2(Graph graph) {
        parent = new HashMap<>();
        this.graph = graph;
        fillParent();
    }

    private void fillParent() {
        for (Node node: graph.getAdjacencySets().keySet()) {
            parent.put(node, new HashSet<>());
        }
    }

    public Graph kruskalAlgorithm() {
        List<Edge> allEdges = getAllEdges();
        List<Edge> MST = new ArrayList<>();

        allEdges.sort(Comparator.comparingInt(Edge::getWeight));
        for (Edge edge : allEdges) {
            if(parent.size() == 1) {
                break;
            }
            Node source = edge.getSource();
            Node destination = edge.getDestination();

            if (!sameList(source, destination)) {
                Set<Node> sourceEdges;
                if (parent.containsKey(source)) {
                    sourceEdges = parent.get(source);
                } else {
                    sourceEdges = parent.get(findParent(source));
                }
                if (sourceEdges != null) {
                    sourceEdges.add(destination);
                    if (parent.get(destination) != null && !parent.get(destination).isEmpty()) {
                        sourceEdges.addAll(parent.get(destination));
                    }
                }
                parent.remove(destination);
                MST.add(edge);
            }
        }

        if (MST.size() == graph.getNumNodes() - 1) {
            return toGraph(MST);
        }
        return null;
    }

    private boolean sameList(Node source, Node destination) {
        if (parent.containsKey(source) && parent.containsKey(destination)) {
            return parent.get(source).contains(destination) && parent.get(destination).contains(source);
        } else if (parent.containsKey(source)) {
            for (Set<Node> nodes : parent.values()) {
                for (Node node : nodes) {
                    if (node == destination) {
                        return true;
                    }
                }
            }
            return parent.get(source).contains(destination);
        } else if (parent.containsKey(destination)) {
            for (Set<Node> nodes : parent.values()) {
                for (Node node : nodes) {
                    if (node == source) {
                        return true;
                    }
                }
            }
            return parent.get(destination).contains(source);
        } else {
            for (Set<Node> nodes : parent.values()) {
                if (nodes.contains(source) && nodes.contains(destination)) {
                    return true;
                }
            }
            return false;
        }
    }

    private Graph toGraph(List<Edge> MST) {
        Graph MSTGraph = new UndirectedGraph();

        for (Node node : graph.getAdjacencySets().keySet()) {
            MSTGraph.addNode(node);
        }

        for (Edge edge : MST) {
            Node source = edge.getSource();
            Node destination = edge.getDestination();
            int weight = edge.getWeight();
            MSTGraph.addEdge(source, destination, weight);
        }

        return MSTGraph;
    }

    private Node findParent(Node destination) {
        for (Set<Node> nodes : parent.values()) {
            for (Node node : nodes) {
                if (node.equals(destination)) {
                    for (Node key : parent.keySet()) {
                        if (parent.get(key) == nodes) {
                            return key;
                        }
                    }
                }
            }
        }

        return null;
    }

    private List<Edge> getAllEdges () {
        List<Edge> edgeList = new ArrayList<>();
        for(Set<Edge> edges : graph.getAdjacencySets().values()) {
            edgeList.addAll(edges);
        }

        return edgeList;
    }
}
