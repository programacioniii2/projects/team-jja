package kruskal.graph;

public class Node {

  private final String element;

  public Node(String element) {
    this.element = element;
  }

  public String getElement() {
    return this.element;
  }

  @Override
  public boolean equals(Object o) {
    Node node = (Node) o;
    return this.element.equals(node.getElement());
  }
}
