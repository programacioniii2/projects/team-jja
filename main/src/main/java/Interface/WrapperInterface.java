package Interface;

import controllers.CoordinateNode;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import kruskal.graph.Edge;
import kruskal.graph.Graph;
import kruskal.graph.Node;
import kruskal.graph.UndirectedGraph;

import java.util.Set;


public class WrapperInterface {
    public static Button customButton(String text, int size) {
        Button button = new Button();
        button.setText(text);
        button.setFont(Font.font("Arial", size));
        button.setCursor(Cursor.HAND);
        button.setStyle("-fx-background-color: #277ee0");
        button.setTextFill(Color.WHITE);
        return button;
    }

    public static void layoutGraph(Set<CoordinateNode> coordinateNodes, double canvasWidth, double canvasHeight, int numIterations, double maxDisplacement) {
        for (CoordinateNode coordinateNode : coordinateNodes) {
            coordinateNode.setX(Math.random() * canvasWidth);
            coordinateNode.setY(Math.random() * canvasHeight);
        }

        double k = Math.sqrt(canvasWidth * canvasHeight / coordinateNodes.size());

        for (int i = 0; i < numIterations; i++) {
            for (CoordinateNode coordinateNode : coordinateNodes) {
                double fx = 0;
                double fy = 0;

                for (CoordinateNode otherCoordinateNode : coordinateNodes) {
                    if (coordinateNode != otherCoordinateNode) {
                        double dx = coordinateNode.getX() - otherCoordinateNode.getX();
                        double dy = coordinateNode.getY() - otherCoordinateNode.getY();
                        double distance = Math.sqrt(dx * dx + dy * dy);
                        double repulsion = k * k / distance;
                        fx += repulsion * dx / distance;
                        fy += repulsion * dy / distance;
                    }
                }

                double distance = Math.sqrt(coordinateNode.getX() * coordinateNode.getX() + coordinateNode.getY() * coordinateNode.getY());
                double attraction = distance * distance / k;
                fx -= attraction * coordinateNode.getX() / distance;
                fy -= attraction * coordinateNode.getY() / distance;

                double displacement = Math.sqrt(fx * fx + fy * fy);
                double could = 0.75;
                if (fx > 0 && fy > 0) {
                    double scale = Math.min(displacement, maxDisplacement) / displacement;
                    double dx = scale * fx * could;
                    double dy = scale * fy * could;
                    coordinateNode.setX(coordinateNode.getX() + dx);
                    coordinateNode.setY(coordinateNode.getY() + dy);
                }
            }
        }
    }

    public static Graph createGraph() {
        Graph graph = new UndirectedGraph();
        Node node1 = new Node("a");
        Node node2 = new Node("b");
        Node node3 = new Node("c");
        Node node4 = new Node("d");
        Node node5 = new Node("e");

        Edge edge1 = new Edge(node1, node2, 20);
        Edge edge2 = new Edge(node2, node3, 10);
        Edge edge3 = new Edge(node3, node4, 14);
        Edge edge4 = new Edge(node4, node5, 5);
        Edge edge5 = new Edge(node1, node3, 21);
        Edge edge6 = new Edge(node2, node4, 12);

        graph.addNode(node1);
        graph.addNode(node2);
        graph.addNode(node3);
        graph.addNode(node4);
        graph.addNode(node5);

        graph.addEdge(node1, node2, 20);
        graph.addEdge(node2, node3, 10);
        graph.addEdge(node3, node4, 14);
        graph.addEdge(node4, node5, 5);
        graph.addEdge(node1, node3, 21);
        graph.addEdge(node2, node4, 12);

        return graph;
    }

}
