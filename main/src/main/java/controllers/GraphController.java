package controllers;

import kruskal.graph.Graph;
import kruskal.graph.Node;

import java.util.HashSet;
import java.util.Set;

public class GraphController {
    Graph graph;
    Set<CoordinateNode> coordinateNodes;
    public GraphController(Graph graph) {
        this.graph = graph;
        coordinateNodes = new HashSet<>();
        fillCoordinatesNode();
    }

    public Set<Node> getNodes() {
        return graph != null ? graph.getAdjacencySets().keySet() : new HashSet<>();
    }

    public void setGraph(Graph graph) {
        this.graph = graph;
    }

    public void fillCoordinatesNode() {
        if (coordinateNodes != null) {
            for (Node node : getNodes()) {
                coordinateNodes.add(new CoordinateNode(node));
            }
        }
    }

    public Node getNode(String element) {
        for (Node node : graph.getAdjacencySets().keySet()) {
            if (node.getElement().equals(element)) {
                return node;
            }
        }
        return null;
    }

    public Set<CoordinateNode> getCoordinateNodes() {
        return coordinateNodes;
    }

    public Graph getGraph() {
        return graph;
    }
    public CoordinateNode getCoordinateNode(Node node) {

        for (CoordinateNode coordinateNode : coordinateNodes) {
            if (coordinateNode.getNode().equals(node)) {
                return coordinateNode;
            }
        }
        return null;
    }

    public void limitsVerification(double canvasWidth, double canvasHeight) {
        for (CoordinateNode coordinateNode : coordinateNodes) {
            double x = coordinateNode.getX();
            double y = coordinateNode.getY();

            if (x >= canvasWidth - 30) {
                coordinateNode.setX(x - 25);
            } else if (x <= 30) {
                coordinateNode.setX(x + 25);
            }

            if (y >= canvasHeight - 30) {
                coordinateNode.setY(y - 25);
            } else if (y <= 30) {
                coordinateNode.setY(y + 25);
            }
        }
    }

    public void setCoordinateNodes(Set<CoordinateNode> coordinateNodes) {
        this.coordinateNodes = coordinateNodes;
    }
}
