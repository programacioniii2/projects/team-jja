package Interface;

import controllers.CoordinateNode;
import controllers.GraphController;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import kruskal.Kruskal2;
import kruskal.graph.Edge;
import kruskal.graph.Graph;
import kruskal.graph.UndirectedGraph;

import java.util.HashSet;
import java.util.Set;

import static Interface.WrapperInterface.*;

public class InterfaceUI extends Application {
  private static final int WINDOW_HEIGHT = 800;
  private static final int WINDOW_WIDTH = 1000;
  private final int CANVAS_WIDTH = 400;
  private final int CANVAS_HEIGHT = 400;
  private final int NUM_ITERATIONS = 500;
  private final double MAX_DISPLACEMENT = 10;
  private HBox root;
  VBox kruskalOptions;
  VBox graphOptions;
  VBox header;
  Canvas graph;
  GraphController graphController;
  GraphWindows graphWindows;


  @Override
  public void init() {
    this.root = new HBox();
    header = new VBox();
    initComponents();
  }

  @Override
  public void start(Stage stage) throws Exception {
    String COLOR_BACKGROUND = "-fx-background-color: rgb(255, 255, 255, 255)";
    header.setStyle(COLOR_BACKGROUND);
    Scene scene = new Scene(header, WINDOW_WIDTH, WINDOW_HEIGHT);
    stage.setTitle("Kruskal Algorithm");
    stage.setScene(scene);
    stage.show();
  }

  public void initComponents() {
    kruskalOptions = new VBox();
    graphController = new GraphController(createGraph());
    graphOptions = new VBox();
    graphWindows = new GraphWindows();
    root.setSpacing(100);
    root.setAlignment(Pos.CENTER);

    drawKruskalOptions();
    drawGraph();
    drawGraphOptions();
    drawHeader();
    header.setAlignment(Pos.TOP_CENTER);
    header.setSpacing(50);
    header.getChildren().addAll(root);
  }

  private void drawHeader() {
    StackPane stackPane = new StackPane();
    Rectangle header = new Rectangle(WINDOW_WIDTH, 150);
    header.setFill(Color.valueOf("#1c66bd"));
    Text title = new Text("Kruskal Algorithm");

    title.setFont(Font.font("Arial", FontWeight.BOLD, 40));
    title.setFill(Color.WHITE);
    stackPane.setAlignment(Pos.CENTER);
    stackPane.getChildren().addAll(header, title);
    this.header.getChildren().add(stackPane);
  }

  public void drawKruskalOptions() {
    kruskalOptions.setAlignment(Pos.TOP_LEFT);
    kruskalOptions.setSpacing(30);
    Button kruskalButton = customButton("Kruskal Algorithm", 15);
    Button clearGraphButton = customButton("Clear graph", 15);

    kruskalButton.setOnAction(event -> {
      Kruskal2 kruskal = new Kruskal2(graphController.getGraph());
      Graph MinExpationGraph = kruskal.kruskalAlgorithm();
      graphController.setGraph(MinExpationGraph);
      root.getChildren().remove(graph);
      drawGraph();
      root.requestLayout();
    });
    clearGraphButton.setOnAction(event -> {
      graphController.setGraph(null);
      root.getChildren().remove(graph);
      drawGraph();
      root.requestLayout();
    });
    kruskalOptions.getChildren().addAll(kruskalButton, clearGraphButton);
    root.getChildren().addAll(kruskalOptions);
  }

  public void drawGraphOptions() {
    graphOptions.setAlignment(Pos.TOP_RIGHT);
    graphOptions.setSpacing(30);
    Button updateGraph = customButton("Update graph", 15);
    Button addNode = customButton("add Node", 15);
    Button addEdge = customButton("add Edge", 15);
    updateGraph.setOnAction(event -> {
      layoutGraph(graphController.getCoordinateNodes(), CANVAS_WIDTH, CANVAS_HEIGHT, NUM_ITERATIONS, MAX_DISPLACEMENT);
      root.getChildren().remove(graph);
      drawGraph();
      root.requestLayout();
    });

    addNode.setOnAction(event -> {
      graphWindows.addNodeWindow(graphController);
      layoutGraph(graphController.getCoordinateNodes(), CANVAS_WIDTH, CANVAS_HEIGHT, NUM_ITERATIONS, MAX_DISPLACEMENT);
      root.getChildren().remove(graph);
      drawGraph();
      root.requestLayout();
    });
    addEdge.setOnAction(event -> {
      graphWindows.addEdgeWindow(graphController);
      layoutGraph(graphController.getCoordinateNodes(), CANVAS_WIDTH, CANVAS_HEIGHT, NUM_ITERATIONS, MAX_DISPLACEMENT);
      root.getChildren().remove(graph);
      drawGraph();
      root.requestLayout();
    });
    graphOptions.setPadding(new Insets(10, 10, 10, 10));
    graphOptions.getChildren().addAll(updateGraph,addNode, addEdge);
    root.getChildren().add(graphOptions);
  }

  public void drawGraph() {

    if(graphController.getGraph() == null) {
      graphController.setGraph(new UndirectedGraph());
      graphController.setCoordinateNodes(new HashSet<>());
    }

    graph = new Canvas(CANVAS_WIDTH, CANVAS_HEIGHT);
    GraphicsContext gc = graph.getGraphicsContext2D();

    layoutGraph(graphController.getCoordinateNodes(), CANVAS_WIDTH, CANVAS_HEIGHT, NUM_ITERATIONS, MAX_DISPLACEMENT);
    graphController.limitsVerification(CANVAS_WIDTH, CANVAS_HEIGHT);
    gc.setFill(Color.RED);
    gc.setLineWidth(5);
    drawNodes(graphController, gc);

    gc.setStroke(Color.BLACK);
    gc.setLineWidth(2);
    drawEdges(graphController, gc);
    gc.strokeRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
    root.getChildren().add(1, graph);
  }

  private void drawNodes(GraphController graphController, GraphicsContext gc) {
    for (CoordinateNode coordinateNode : graphController.getCoordinateNodes()) {
      gc.fillOval (coordinateNode.getX() - 10, coordinateNode.getY() - 10, 20, 20);
      gc.fillText (String.valueOf(coordinateNode.getNode().getElement()), coordinateNode.getX()-20, coordinateNode.getY()-20);
    }
  }

  private void drawEdges(GraphController graphController, GraphicsContext gc) {
    for (Set<Edge> edges : graphController.getGraph().getAdjacencySets().values()) {
      for (Edge edge : edges) {
        CoordinateNode source = graphController.getCoordinateNode(edge.getSource());
        CoordinateNode destination = graphController.getCoordinateNode(edge.getDestination());
        assert source != null;
        assert destination != null;
        gc.strokeLine (source.getX(), source.getY(), destination.getX(), destination.getY());
      }
    }
  }


  public static void main(String[] args) {
    launch(args);
  }
}
