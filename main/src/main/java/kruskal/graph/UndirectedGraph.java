package kruskal.graph;

import java.util.Set;

public class UndirectedGraph extends Graph {

  public UndirectedGraph() {
    super();
  }

  @Override
  public boolean addEdge(Node node1, Node node2, int weight) {
    addNode(node1);
    addNode(node2);
    boolean addEdgeSuccess = (addEdgeFromTo(node1, node2, weight)
            && addEdgeFromTo(node2, node1, weight));
    if (addEdgeSuccess) {
      super.numEdges++;
    }
    return addEdgeSuccess;
  }

  @Override
  public boolean addEdge(Edge edge) {
    Node node1 = edge.getSource();
    Node node2 = edge.getDestination();
    int weight = edge.getWeight();
    addNode(node1);
    addNode(node2);
    boolean addEdgeSuccess = addEdgeFromTo(node1, node2, weight);
    if (addEdgeSuccess) {
      super.numEdges++;
    }
    return addEdgeSuccess;
  }

  @Override
  public boolean removeEdge(Node node1, Node node2, int weight) {
    Edge e1 = new Edge(node1, node2, weight);
    Edge e2 = new Edge(node2, node1, weight);
    boolean result1 = false;
    boolean result2 = false;
    for (Node current : this.adjacencySets.keySet()) {
      Set<Edge> edges = this.adjacencySets.get(current);
      if (!result1) {
        result1 = edges.removeIf(e -> e.equals(e1));
      }
      if (!result2) {
        result2 = edges.removeIf(e -> e.equals(e2));
      }
    }
    return result1 && result2;
  }

  @Override
  public boolean updateEdge(Node node1, Node node2, int weight, int newWeight) {
    Edge e1 = new Edge(node1, node2, weight);
    Edge e2 = new Edge(node2, node1, weight);
    boolean result1 = false;
    boolean result2 = false;
    for (Node current : this.adjacencySets.keySet()) {
      Set<Edge> edges = this.adjacencySets.get(current);
      if (!result1 && edges.contains(e1)) {
        edges.stream().filter(e -> e.equals(e1)).findFirst().orElseThrow().setWeight(newWeight);
        result1 = true;
      }
      if (!result2 && edges.contains(e2)) {
        edges.stream().filter(e -> e.equals(e2)).findFirst().orElseThrow().setWeight(newWeight);
        result2 = true;
      }
      if (result1 && result2) return true;
    }
    return false;
  }
}
