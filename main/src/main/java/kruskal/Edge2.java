package kruskal;

public class Edge2 {
  int src, dest, weight;

  public Edge2(int src, int dest, int weight) {
    this.src = src;
    this.dest = dest;
    this.weight = weight;
  }

  @Override
  public String toString() {
    return "(" + src + ", " + dest + ", " + weight + ")";
  }
}